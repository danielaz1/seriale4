package db.repository;

import db.mapper.AbstractMapper;
import db.uow.IUnitOfWorkRepository;
import db.uow.UnitOfWork;
import domain.Entity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractRepository<TEntity extends Entity> implements Repository<TEntity>, IUnitOfWorkRepository {

	protected Connection connection;
	protected AbstractMapper<TEntity> mapper;
	protected UnitOfWork unitOfWork;

	public AbstractRepository(Connection connection, UnitOfWork unitOfWork) {
		this.connection = connection;
		this.unitOfWork = unitOfWork;
		try {
			if (! tableExists()) {
				Statement createTable = connection.createStatement();
				createTable.executeUpdate(getCreateTableSql());
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private boolean tableExists() throws SQLException {
		ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
		while (rs.next()) {
			if (getTableName().equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
				return true;
			}
		}
		return false;
	}

	protected abstract String getTableName();

	protected abstract String getCreateTableSql();

	@Override
	public void persistAdd(Entity e) {
		mapper.add((TEntity) e);
	}

	@Override
	public void persistUpdate(Entity e) {
		mapper.update((TEntity) e);
	}

	@Override
	public void persistDelete(Entity e) {
		mapper.remove((TEntity) e);
	}

	@Override
	public void add(TEntity entity) {
		unitOfWork.markAsNew(entity, this);
	}

	@Override
	public void modify(TEntity entity) {
		unitOfWork.markAsDirty(entity, this);
	}

	@Override
	public void remove(TEntity entity) {
		unitOfWork.markAsDeleted(entity, this);
	}
}
