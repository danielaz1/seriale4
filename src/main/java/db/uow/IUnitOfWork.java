package db.uow;

import domain.Entity;

public interface IUnitOfWork {

	void commit();

	void rollback();

	void markAsNew(Entity e, IUnitOfWorkRepository repository);

	void markAsDirty(Entity e, IUnitOfWorkRepository repository);

	void markAsDeleted(Entity e, IUnitOfWorkRepository repository);

}
