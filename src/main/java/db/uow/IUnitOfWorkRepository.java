package db.uow;

import domain.Entity;

public interface IUnitOfWorkRepository {

	void persistAdd(Entity e);

	void persistUpdate(Entity e);

	void persistDelete(Entity e);

}
