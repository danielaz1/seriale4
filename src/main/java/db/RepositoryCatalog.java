package db;

import db.repository.*;
import db.uow.UnitOfWork;

import java.sql.Connection;

public class RepositoryCatalog {

	private Connection connection;
	private UnitOfWork unitOfWork;
	private ActorRepository actorRepository;
	private DirectorRepository directorRepository;
	private EpisodeRepository episodeRepository;
	private SeasonRepository seasonRepository;
	private TvSeriesRepository tvSeriesRepository;

	public RepositoryCatalog(Connection connection, UnitOfWork unitOfWork) {
		this.connection = connection;
		this.unitOfWork = unitOfWork;
		actorRepository = new ActorRepository(connection, unitOfWork);
		directorRepository = new DirectorRepository(connection, unitOfWork);
		episodeRepository = new EpisodeRepository(connection, unitOfWork);
		seasonRepository = new SeasonRepository(connection, unitOfWork);
		tvSeriesRepository = new TvSeriesRepository(connection, unitOfWork);
	}

	public ActorRepository actors() {
		return actorRepository;
	}

	public DirectorRepository directors() {
		return directorRepository;
	}

	public EpisodeRepository episodes() {
		return episodeRepository;
	}

	public SeasonRepository seasons() {
		return seasonRepository;
	}

	public TvSeriesRepository tvSeries() {
		return tvSeriesRepository;
	}
}
