package db.mapper;

import domain.Entity;

import java.sql.*;
import java.util.ArrayList;

public abstract class AbstractMapper<T extends Entity> {

	protected Connection connection;
	protected PreparedStatement addStatement;

	abstract protected String findStatement();

	abstract protected String getAllStatement();

	abstract protected String insertStatement();

	abstract protected String updateStatement();

	abstract protected String removeStatement();

	protected abstract String selectId();

	abstract protected T doLoad(ResultSet rs) throws SQLException;

	abstract protected void parametrizeInsertStatement(PreparedStatement statement, T entity) throws SQLException;

	abstract protected void parametrizeUpdateStatement(PreparedStatement statement, T entity) throws SQLException;

	protected AbstractMapper(Connection connection) {
		this.connection = connection;
		try {
			addStatement = connection.prepareStatement(insertStatement(), Statement.RETURN_GENERATED_KEYS);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public T find(long id) {
		PreparedStatement findStatement;
		try {
			findStatement = connection.prepareStatement(findStatement());
			findStatement.setLong(1, id);
			ResultSet rs = findStatement.executeQuery();
			if (rs.next()) {
				return doLoad(rs);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	public ArrayList<T> findAll() {
		ArrayList<T> results = new ArrayList<>();
		PreparedStatement getAllStmt;
		try {
			getAllStmt = connection.prepareStatement(getAllStatement());
			ResultSet rs = getAllStmt.executeQuery();
			while (rs.next()) {
				T result = doLoad(rs);
				results.add(result);
			}
			return results;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public long add(T entity) {
		long key = 0;
		try {
			parametrizeInsertStatement(addStatement, entity);
			addStatement.executeUpdate();
			ResultSet generatedKeys = addStatement.getGeneratedKeys();
			if (generatedKeys.next()) {
				key = generatedKeys.getLong(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return key;
	}

	public void update(T entity) {
		PreparedStatement updateStatement;
		try {
			updateStatement = connection.prepareStatement(updateStatement());

			parametrizeUpdateStatement(updateStatement, entity);

			updateStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void remove(long id) {
		PreparedStatement removeStatement;
		try {
			removeStatement = connection.prepareStatement(removeStatement());

			removeStatement.setLong(1, id);

			removeStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void remove(T entity) {
		remove(entity.getId());
	}

	public void removeAll() {
		try {
			Statement selectAllIdStmt = connection.createStatement();
			ResultSet rs = selectAllIdStmt.executeQuery(selectId());
			while (rs.next()) {
				long id = rs.getLong("id");
				remove(id);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
