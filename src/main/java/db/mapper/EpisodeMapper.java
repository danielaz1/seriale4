package db.mapper;

import domain.Episode;

import java.sql.*;
import java.time.Duration;
import java.util.ArrayList;

public class EpisodeMapper extends AbstractMapper<Episode> {

	private static final String SELECT_BY_ID_SQL = "SELECT * FROM episode where id=?";
	private static final String SELECT_SQL = "SELECT * FROM episode";
	private static final String INSERT_SQL = "INSERT INTO episode (name, number, duration, release_date, season_id) VALUES (?,?,?,?,?)";
	private static final String UPDATE_SQL = "UPDATE episode SET name=?, number=?, duration=?, release_date=? WHERE ID=?";
	private static final String DELETE_SQL = "Delete from episode where id=?";
	private static final String SELECT_BY_SEASON_ID_SQL = "SELECT * FROM episode WHERE SEASON_ID=?";
	private static final String SELECT_ID = "select id from episode";

	public EpisodeMapper(Connection connection) {
		super(connection);
	}

	@Override
	protected String findStatement() {
		return SELECT_BY_ID_SQL;
	}

	@Override
	protected String getAllStatement() {
		return SELECT_SQL;
	}

	@Override
	protected String insertStatement() {
		return INSERT_SQL;
	}

	@Override
	protected String updateStatement() {
		return UPDATE_SQL;
	}

	@Override
	protected String removeStatement() {
		return DELETE_SQL;
	}

	@Override
	protected String selectId() {
		return SELECT_ID;
	}

	@Override
	protected Episode doLoad(ResultSet rs) throws SQLException {
		Episode episode = new Episode();
		episode.setName(rs.getString("name"));
		episode.setEpisodeNumber(rs.getInt("number"));
		episode.setDuration(Duration.ofSeconds(rs.getInt("duration")));
		episode.setReleaseDate(rs.getDate("release_date").toLocalDate());
		episode.setId(rs.getLong("id"));
		return episode;
	}

	@Override
	protected void parametrizeInsertStatement(PreparedStatement statement, Episode episode) throws SQLException {
		statement.setString(1, episode.getName());
		statement.setInt(2, episode.getEpisodeNumber());
		statement.setLong(3, episode.getDuration().getSeconds());
		statement.setDate(4, java.sql.Date.valueOf(episode.getReleaseDate()));
	}

	@Override
	protected void parametrizeUpdateStatement(PreparedStatement statement, Episode episode) throws SQLException {
		parametrizeInsertStatement(statement, episode);
		statement.setLong(5, episode.getId());
	}

	public ArrayList<Episode> getBySeasonId(long seasonId) throws SQLException {
		ArrayList<Episode> episodes = new ArrayList<>();
		PreparedStatement selectBySeasonIdStmt = connection.prepareStatement(SELECT_BY_SEASON_ID_SQL);
		selectBySeasonIdStmt.setLong(1, seasonId);
		ResultSet rs = selectBySeasonIdStmt.executeQuery();
		while (rs.next()) {
			Episode episode = doLoad(rs);
			episodes.add(episode);
		}
		return episodes;
	}

	public long add(Episode episode, Long seasonId) throws SQLException {
		if (seasonId != null) {
			addStatement.setLong(5, seasonId);
		} else {
			addStatement.setNull(5, Types.BIGINT);
		}
		return super.add(episode);
	}

	@Override
	public long add(Episode episode) {
		try {
			return add(episode, null);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
