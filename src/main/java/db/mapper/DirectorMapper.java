package db.mapper;

import domain.Director;

import java.sql.*;

public class DirectorMapper extends AbstractMapper<Director> {

	private static final String SELECT_BY_TVS_ID_SQL = "SELECT * FROM DIRECTOR WHERE tvs_id=?";
	private static final String INSERT_SQL = "insert into DIRECTOR (NAME, DATE_OF_BIRTH, BIOGRAPHY, TVS_ID) values (?,?,?,?)";
	private static final String SELECT_BY_ID_SQL = "select * from director where id=?";
	private static final String SELECT_SQL = "select * from director";
	private static final String UPDATE_SQL = "UPDATE director SET name=?, date_of_birth=?, biography=? WHERE id=?";
	private static final String DELETE_SQL = "Delete from director where id=?";
	private static final String SELECT_ID = "select id from director";

	public DirectorMapper(Connection connection) {
		super(connection);
	}

	@Override
	protected String findStatement() {
		return SELECT_BY_ID_SQL;
	}

	@Override
	protected String getAllStatement() {
		return SELECT_SQL;
	}

	@Override
	protected String insertStatement() {
		return INSERT_SQL;
	}

	@Override
	protected String updateStatement() {
		return UPDATE_SQL;
	}

	@Override
	protected String removeStatement() {
		return DELETE_SQL;
	}

	@Override
	protected String selectId() {
		return SELECT_ID;
	}

	@Override
	protected Director doLoad(ResultSet rs) throws SQLException {
		Director director = new Director();
		director.setName(rs.getString("name"));
		director.setDateOfBirth(rs.getDate("date_of_birth").toLocalDate());
		director.setBiography(rs.getString("biography"));
		director.setId(rs.getLong("id"));
		return director;
	}

	@Override
	protected void parametrizeInsertStatement(PreparedStatement statement, Director director) throws SQLException {
		statement.setString(1, director.getName());
		statement.setDate(2, Date.valueOf(director.getDateOfBirth()));
		statement.setString(3, director.getBiography());
	}

	@Override
	protected void parametrizeUpdateStatement(PreparedStatement statement, Director director) throws SQLException {
		parametrizeInsertStatement(statement, director);
		statement.setLong(4, director.getId());
	}

	Director getDirectorByTvsId(long tvsId) throws SQLException {
		Director director = null;
		PreparedStatement selectByTvsIdStmt = connection.prepareStatement(SELECT_BY_TVS_ID_SQL);
		selectByTvsIdStmt.setLong(1, tvsId);
		ResultSet rs = selectByTvsIdStmt.executeQuery();
		if (rs.next()) {
			director = doLoad(rs);
		}
		return director;
	}

	@Override
	public long add(Director director) {
		try {
			return add(director, null);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	long add(Director director, Long tvsId) throws SQLException {
		if (tvsId != null) {
			addStatement.setLong(4, tvsId);
		} else {
			addStatement.setNull(4, Types.BIGINT);
		}
		return super.add(director);
	}
}

