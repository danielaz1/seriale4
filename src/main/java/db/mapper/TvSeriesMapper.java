package db.mapper;

import domain.Season;
import domain.TvSeries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TvSeriesMapper extends AbstractMapper<TvSeries> {

	private static final String FIND_BY_ID_SQL = "Select * from tv_series where id=?";
	private static final String FIND_ALL_SQL = "Select * from tv_series";
	private static final String INSERT_SQL = "Insert into tv_series (name) values (?)";
	private static final String UPDATE_SQL = "Update tv_series set name=? where id=?";
	private static final String DELETE_SQL = "Delete from tv_series where id=?";
	private static final String SELECT_ID = "select id from tv_series";

	private TvSeriesActorMapper tvSeriesActorMapper;
	private SeasonMapper seasonMapper;
	private DirectorMapper directorMapper;

	public TvSeriesMapper(Connection connection) {
		super(connection);
		tvSeriesActorMapper = new TvSeriesActorMapper(connection);
		seasonMapper = new SeasonMapper(connection);
		directorMapper = new DirectorMapper(connection);
	}

	@Override
	protected String findStatement() {
		return FIND_BY_ID_SQL;
	}

	@Override
	protected String getAllStatement() {
		return FIND_ALL_SQL;
	}

	@Override
	protected String insertStatement() {
		return INSERT_SQL;
	}

	@Override
	protected String updateStatement() {
		return UPDATE_SQL;
	}

	@Override
	protected String removeStatement() {
		return DELETE_SQL;
	}

	@Override
	protected String selectId() {
		return SELECT_ID;
	}

	@Override
	protected TvSeries doLoad(ResultSet rs) throws SQLException {
		long id = rs.getLong("id");
		TvSeries tvs = new TvSeries();
		tvs.setId(id);
		tvs.setName(rs.getString("name"));

		tvs.setSeasons(seasonMapper.getSeasonsByTvsId(id));

		tvs.setDirector(directorMapper.getDirectorByTvsId(id));

		tvs.setActors(tvSeriesActorMapper.getActorsByTvsID(id));

		return tvs;
	}

	@Override
	protected void parametrizeInsertStatement(PreparedStatement statement, TvSeries tvSeries) throws SQLException {
		statement.setString(1, tvSeries.getName());
	}

	@Override
	protected void parametrizeUpdateStatement(PreparedStatement statement, TvSeries tvSeries) throws SQLException {
		parametrizeInsertStatement(statement, tvSeries);
		statement.setLong(2, tvSeries.getId());
	}

	public long add(TvSeries tvSeries) {
		long tvsId = super.add(tvSeries);
		try {
			if (tvSeries.getSeasons() != null) {
				ArrayList<Season> seasons = tvSeries.getSeasons();
				for (Season season : seasons) {
					seasonMapper.add(season, tvsId);
				}
			}

			if (tvSeries.getDirector() != null) {
				directorMapper.add(tvSeries.getDirector(), tvsId);
			}

			if (tvSeries.getActors() != null) {
				tvSeriesActorMapper.addActors(tvSeries.getActors(), tvsId);

			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return tvsId;
	}

	@Override
	public void remove(long id) {
		try {
			tvSeriesActorMapper.removeByTvsId(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		super.remove(id);
	}

}
