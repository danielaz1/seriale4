package db.mapper;

import domain.Episode;
import domain.Season;

import java.sql.*;
import java.util.ArrayList;

public class SeasonMapper extends AbstractMapper<Season> {

	private static final String SELECT_BY_ID_SQL = "SELECT id, number, year_of_release FROM season where id=?";
	private static final String SELECT_SQL = "SELECT id, number, year_of_release FROM season";
	private static final String INSERT_SQL = "INSERT INTO season (number, year_of_release, tvs_id) VALUES (?,?,?)";
	private static final String UPDATE_SQL = "UPDATE season SET number=?, year_of_release=? WHERE id=?";
	private static final String DELETE_SQL = "Delete from season where id=?";
	private static final String SELECT_BY_TVS_ID_SQL = "SELECT * FROM SEASON WHERE TVS_ID=?";
	private static final String SELECT_ID = "select id from season";

	private EpisodeMapper episodeMapper;

	public SeasonMapper(Connection connection) {
		super(connection);
		episodeMapper = new EpisodeMapper(connection);
	}

	@Override
	protected String findStatement() {
		return SELECT_BY_ID_SQL;
	}

	@Override
	protected String getAllStatement() {
		return SELECT_SQL;
	}

	@Override
	protected String insertStatement() {
		return INSERT_SQL;
	}

	@Override
	protected String updateStatement() {
		return UPDATE_SQL;
	}

	@Override
	protected String removeStatement() {
		return DELETE_SQL;
	}

	@Override
	protected String selectId() {
		return SELECT_ID;
	}

	@Override
	protected Season doLoad(ResultSet rs) throws SQLException {
		Season season = new Season();
		long seasonId = (rs.getLong("id"));
		season.setId(seasonId);
		season.setSeasonNumber(rs.getInt("number"));
		season.setYearOfRelease(rs.getInt("year_of_release"));

		season.setEpisodes(episodeMapper.getBySeasonId(seasonId));

		return season;
	}

	@Override
	protected void parametrizeInsertStatement(PreparedStatement statement, Season season) throws SQLException {
		statement.setInt(1, season.getSeasonNumber());
		statement.setInt(2, season.getYearOfRelease());
	}

	@Override
	protected void parametrizeUpdateStatement(PreparedStatement statement, Season season) throws SQLException {
		parametrizeInsertStatement(statement, season);
		statement.setLong(3, season.getId());
	}

	ArrayList<Season> getSeasonsByTvsId(long tvsId) throws SQLException {
		ArrayList<Season> seasons = new ArrayList<>();
		PreparedStatement selectByTvsIdStmt = connection.prepareStatement(SELECT_BY_TVS_ID_SQL);
		selectByTvsIdStmt.setLong(1, tvsId);
		ResultSet rs = selectByTvsIdStmt.executeQuery();
		while (rs.next()) {
			Season season = doLoad(rs);
			seasons.add(season);
		}
		return seasons;
	}

	long add(Season season, Long tvsId) throws SQLException {
		if (tvsId != null) {
			addStatement.setLong(3, tvsId);
		} else {
			addStatement.setNull(3, Types.BIGINT);
		}
		long seasonId = super.add(season);
		if (season.getEpisodes() != null) {
			ArrayList<Episode> episodes = season.getEpisodes();
			for (Episode episode : episodes) {
				episodeMapper.add(episode, seasonId);
			}
		}
		return seasonId;
	}

	@Override
	public long add(Season season) {
		try {
			return add(season, null);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
