package db.mapper;

import domain.Actor;

import java.sql.*;
import java.util.ArrayList;

public class TvSeriesActorMapper {

	private Connection connection;
	private PreparedStatement insertStatement;

	private static final String DELETE_BY_TVS_ID = "DELETE FROM TVSERIES_ACTOR WHERE TVS_ID =?";
	private static final String DELETE_BY_ACTOR_ID = "DELETE FROM TVSERIES_ACTOR WHERE ACTOR_ID=?";
	private static final String TABLE_NAME = "TVSERIES_ACTOR";
	private static final String CREATE_TABLE = "CREATE TABLE tvseries_actor (tvs_id BIGINT, actor_id BIGINT)";
	private static final String SELECT_ACTORS_ID = "SELECT ACTOR_ID FROM TVSERIES_ACTOR WHERE TVS_ID=?";
	private static final String INSERT = "INSERT INTO TVSERIES_ACTOR (TVS_ID, ACTOR_ID) VALUES (?,?)";

	TvSeriesActorMapper(Connection connection) {
		this.connection = connection;
		try {
			Statement statement = connection.createStatement();
			if (! tableExists()) {
				statement.executeUpdate(CREATE_TABLE);
			}
			insertStatement = connection.prepareStatement(INSERT);
		} catch (SQLException e) {
			e.printStackTrace();

		}
	}

	private boolean tableExists() throws SQLException {
		ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
		while (rs.next()) {
			if (TABLE_NAME.equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
				return true;
			}
		}
		return false;
	}

	ArrayList<Actor> getActorsByTvsID(long tvSeriesId) throws SQLException {
		ActorMapper actorMapper = new ActorMapper(connection);
		ArrayList<Actor> actors = new ArrayList<>();

		PreparedStatement selectActorsIdStmt = connection.prepareStatement(SELECT_ACTORS_ID);
		selectActorsIdStmt.setLong(1, tvSeriesId);

		ResultSet rs = selectActorsIdStmt.executeQuery();

		while (rs.next()) {
			long actorId = rs.getLong("actor_id");
			Actor actor = actorMapper.find(actorId);
			actors.add(actor);
		}
		return actors;
	}

	void addActors(ArrayList<Actor> actors, long tvSeriesId) throws SQLException {
		ActorMapper actorMapper = new ActorMapper(connection);
		for (Actor actor : actors) {
			long actorId = actorMapper.add(actor);
			insertStatement.setLong(1, tvSeriesId);
			insertStatement.setLong(2, actorId);
			insertStatement.executeUpdate();
		}
	}

	void removeByTvsId(long tvSeriesId) throws SQLException {
		PreparedStatement deleteByTvsIdStmt = connection.prepareStatement(DELETE_BY_TVS_ID);
		deleteByTvsIdStmt.setLong(1, tvSeriesId);
		deleteByTvsIdStmt.executeUpdate();
	}

	void removeByActorId(long actorId) throws SQLException {
		PreparedStatement deleteByActorIdStmt = connection.prepareStatement(DELETE_BY_ACTOR_ID);
		deleteByActorIdStmt.setLong(1, actorId);
		deleteByActorIdStmt.executeUpdate();
	}
}
