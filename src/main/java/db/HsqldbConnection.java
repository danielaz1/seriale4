package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class HsqldbConnection {

	private String url = "jdbc:hsqldb:hsql://localhost/testdb";
	private Connection connection;

	public HsqldbConnection() {
		try {
			connection = DriverManager.getConnection(url);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Connection getConnection() {
		return connection;
	}

}
