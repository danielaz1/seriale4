package domain;

import java.util.ArrayList;

public class Season extends Entity {

	private int seasonNumber;
	private int yearOfRelease;
	private ArrayList<Episode> episodes;

	public Season(int seasonNumber, int yearOfRelease) {
		this.seasonNumber = seasonNumber;
		this.yearOfRelease = yearOfRelease;
	}

	public Season() {
	}

	public int getSeasonNumber() {
		return seasonNumber;
	}

	public void setSeasonNumber(int seasonNumber) {
		this.seasonNumber = seasonNumber;
	}

	public int getYearOfRelease() {
		return yearOfRelease;
	}

	public void setYearOfRelease(int yearOfRelease) {
		this.yearOfRelease = yearOfRelease;
	}

	public ArrayList<Episode> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(ArrayList<Episode> episodes) {
		this.episodes = episodes;
	}

}
