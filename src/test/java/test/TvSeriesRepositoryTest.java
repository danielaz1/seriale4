package test;

import db.HsqldbConnection;
import db.RepositoryCatalog;
import db.repository.TvSeriesRepository;
import db.uow.UnitOfWork;
import domain.*;
import org.junit.Test;

import java.sql.Connection;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;

import static java.time.LocalDate.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TvSeriesRepositoryTest {

	private static final LocalDate DATE = of(1990, 1, 1);
	private final static String NAME_1 = "Serial1";
	private final static String NAME_2 = "Serial2";

	private TvSeriesRepository tvSeriesRepository;

	@Test
	public void checkConnection() {
		assertNotNull(new HsqldbConnection().getConnection());
	}

	@Test
	public void checkOperations() {
		Connection connection = new HsqldbConnection().getConnection();
		UnitOfWork unitOfWork = new UnitOfWork(connection);
		RepositoryCatalog repositoryCatalog = new RepositoryCatalog(connection, unitOfWork);
		tvSeriesRepository = repositoryCatalog.tvSeries();

		tvSeriesRepository.removeAll();

		TvSeries tvSeries = new TvSeries(NAME_1);
		tvSeriesRepository.add(tvSeries);
		unitOfWork.commit();

		ArrayList<TvSeries> tvs = tvSeriesRepository.getAll();
		TvSeries tvsRetrieved = tvs.get(0);
		assertEquals(NAME_1, tvsRetrieved.getName());

		tvsRetrieved.setName(NAME_2);
		tvSeriesRepository.modify(tvsRetrieved);
		unitOfWork.commit();

		TvSeries tvsRetrieved2 = tvSeriesRepository.withId(tvsRetrieved.getId());
		assertEquals(NAME_2, tvsRetrieved2.getName());

		tvSeriesRepository.remove(tvsRetrieved2);
		unitOfWork.commit();

		tvs = tvSeriesRepository.getAll();
		assertEquals(0, tvs.size());
	}

	@Test
	public void checkTvs() {
		Connection connection = new HsqldbConnection().getConnection();
		UnitOfWork unitOfWork = new UnitOfWork(connection);
		RepositoryCatalog repositoryCatalog = new RepositoryCatalog(connection, unitOfWork);
		tvSeriesRepository = repositoryCatalog.tvSeries();

		tvSeriesRepository.removeAll();

		Episode episode = new Episode(NAME_1, 1, Duration.ofSeconds(9999), DATE);
		ArrayList<Episode> episodes = new ArrayList<>();
		episodes.add(episode);

		Season season = new Season(1, 2010);
		season.setEpisodes(episodes);
		ArrayList<Season> seasons = new ArrayList<>();
		seasons.add(season);

		Actor actor = new Actor();
		actor.setName(NAME_1);
		actor.setDateOfBirth(DATE);
		ArrayList<Actor> actors = new ArrayList<>();
		actors.add(actor);

		Director director = new Director();
		director.setName(NAME_1);
		director.setDateOfBirth(DATE);

		TvSeries tvSeries = new TvSeries(NAME_1);
		tvSeries.setSeasons(seasons);
		tvSeries.setActors(actors);
		tvSeries.setDirector(director);

		tvSeriesRepository.add(tvSeries);
		unitOfWork.commit();

		ArrayList<TvSeries> tvs = tvSeriesRepository.getAll();
		TvSeries tvsRetrieved = tvs.get(0);

		actors = tvsRetrieved.getActors();
		assertNotNull(actors);
		assertEquals(1, actors.size());

		actor = actors.get(0);
		assertEquals(NAME_1, actor.getName());
		assertEquals(DATE, actor.getDateOfBirth());

		director = tvsRetrieved.getDirector();
		assertNotNull(director);
		assertEquals(NAME_1, director.getName());
		assertEquals(DATE, director.getDateOfBirth());

		seasons = tvsRetrieved.getSeasons();
		assertNotNull(seasons);
		assertEquals(1, seasons.size());

		Season seasonRetrieved = seasons.get(0);
		episodes = seasonRetrieved.getEpisodes();
		assertNotNull(episodes);
		assertEquals(1, episodes.size());

		tvSeriesRepository.remove(tvsRetrieved);
		unitOfWork.commit();

		tvs = tvSeriesRepository.getAll();
		assertEquals(0, tvs.size());
	}

}
