package test;

import db.HsqldbConnection;
import db.RepositoryCatalog;
import db.repository.EpisodeRepository;
import db.uow.UnitOfWork;
import domain.Episode;
import org.junit.Test;

import java.sql.Connection;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class EpisodeRepositoryTest {

	private final static String NAME_1 = "Name1";
	private final static int NUMBER_1 = 1;
	private final static int NUMBER_2 = 2;
	private final static LocalDate DATE_1 = LocalDate.of(1990, 1, 1);
	private final static Duration DURATION_1 = Duration.ofSeconds(9999);

	@Test
	public void checkOperations() {
		Connection connection = new HsqldbConnection().getConnection();
		UnitOfWork unitOfWork = new UnitOfWork(connection);
		RepositoryCatalog repositoryCatalog = new RepositoryCatalog(connection, unitOfWork);
		EpisodeRepository episodeRepository = repositoryCatalog.episodes();
		episodeRepository.removeAll();

		Episode episode = new Episode(NAME_1, NUMBER_1, DURATION_1, DATE_1);
		episodeRepository.add(episode);
		unitOfWork.commit();

		ArrayList<Episode> episodes = episodeRepository.getAll();
		Episode episodeRetrieved = episodes.get(0);
		assertEquals(NUMBER_1, episodeRetrieved.getEpisodeNumber());
		assertEquals(DATE_1, episodeRetrieved.getReleaseDate());
		assertEquals(DURATION_1, episodeRetrieved.getDuration());

		episodeRetrieved.setEpisodeNumber(NUMBER_2);
		episodeRepository.modify(episodeRetrieved);
		unitOfWork.commit();

		Episode episodeRetrieved2 = episodeRepository.withId(episodeRetrieved.getId());
		assertEquals(NUMBER_2, episodeRetrieved2.getEpisodeNumber());
		assertEquals(episodeRetrieved.getId(), episodeRetrieved2.getId());

		episodeRepository.remove(episodeRetrieved2);
		unitOfWork.commit();

		episodes = episodeRepository.getAll();
		assertEquals(0, episodes.size());
	}

}
