package test;

import db.HsqldbConnection;
import db.RepositoryCatalog;
import db.repository.DirectorRepository;
import db.uow.UnitOfWork;
import domain.Director;
import org.junit.Test;

import java.sql.Connection;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class DirectorRepositoryTest {

	private final static String NAME_1 = "Name1";
	private final static String NAME_2 = "Name2";
	private final static String BIOGRAPHY_1 = "Biography1";
	private final static LocalDate DATE_1 = LocalDate.of(1990, 1, 1);

	@Test
	public void checkOperations() {
		Connection connection = new HsqldbConnection().getConnection();
		UnitOfWork unitOfWork = new UnitOfWork(connection);
		RepositoryCatalog repositoryCatalog = new RepositoryCatalog(connection, unitOfWork);

		DirectorRepository directorRepository = repositoryCatalog.directors();
		directorRepository.removeAll();
		Director director = new Director(NAME_1, DATE_1, BIOGRAPHY_1);
		directorRepository.add(director);
		unitOfWork.commit();

		//directorRepository.persistAdd(director);

		ArrayList<Director> directors = directorRepository.getAll();
		Director directorRetrieved = directors.get(0);
		assertEquals(NAME_1, directorRetrieved.getName());
		assertEquals(BIOGRAPHY_1, directorRetrieved.getBiography());
		assertEquals(DATE_1, directorRetrieved.getDateOfBirth());

		directorRetrieved.setName(NAME_2);
		directorRepository.modify(directorRetrieved);
		unitOfWork.commit();
		Director directorRetrieved2 = directorRepository.withId(directorRetrieved.getId());
		assertEquals(NAME_2, directorRetrieved2.getName());
		assertEquals(directorRetrieved.getId(), directorRetrieved2.getId());

		directorRepository.remove(directorRetrieved2);
		unitOfWork.commit();

		directors = directorRepository.getAll();
		assertEquals(0, directors.size());
	}

}
